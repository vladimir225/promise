const { assert, expect } = require('chai');
const MyPromise = require('../src/promise');
const spies = require('chai-spies');
const chai = require('chai');
chai.use(spies);

describe('25.4.5.3 Promise.prototype.then', function() {
  it('is a function', function() {
    assert.typeOf(MyPromise.prototype.then, 'function');
  });

  it('throws TypeError if this is not a Promise', function() {
    const obj = {};
    const p = new MyPromise(res => {
      res();
    });

    assert.throws(function() {
      p.then.call(obj, r => r);
    }, TypeError);
  });

  it('expects this to be a Promise', function(done) {
    const p = new MyPromise(res => res());

    MyPromise.prototype.then
      .call(p, function() {
        assert.instanceOf(p, MyPromise);
      })
      .then(done)
      .catch(done);
  });

  it('has default on resolve: identity', function(done) {
    const value = 10;
    const p = new Promise(function(resolve, reject) {
      resolve(value);
    });

    p.then(val => {
      assert.equal(value, val);
    })
      .then(done)
      .catch(done);
  });

  it('does call onFulfilled immediately if promise status is fulfilled', function(done) {
    const promise = new MyPromise(res => {
      res();
    });
    const onFulfilled = chai.spy();

    promise
      .then(onFulfilled)
      .then(() => {
        expect(onFulfilled).to.have.been.called();
      })
      .then(done)
      .catch(done);
  });
  it('never calls onRejected if promise status is "fulfilled"', function(done) {
    const p = new MyPromise(resolve => resolve());
    const onRejected = chai.spy();
    p.then(() => null, onRejected)
      .then(() => {
        expect(onRejected).to.not.have.been.called();
      })
      .then(done)
      .catch(done);
  });
  it('never calls onFulfilled if promise status is "rejected"', function(done) {
    const p = new MyPromise((resolve, reject) => reject());
    const onFulfilled = chai.spy();
    p.then(onFulfilled, null)
      .then(() => {
        expect(onFulfilled).to.not.have.been.called();
      })
      .then(done)
      .catch(done);
  });

  it('does not call either function immediately if promise status is pending', function() {
    const p = new MyPromise(function() {});
    const onFulfilled = chai.spy();
    const onRejected = chai.spy();
    p.then(onFulfilled, onRejected);

    expect(onFulfilled).to.not.have.been.called();
    expect(onRejected).to.not.have.been.called();
  });

  it('does call onRejected immediately if promise status is rejected', function(done) {
    const p = new MyPromise((res, rej) => {
      rej();
    });

    const onRejected = chai.spy();

    p.then(null, onRejected)
      .then(() => {
        expect(onRejected).to.have.been.called();
      })
      .then(done)
      .catch(done);
  });
});

describe('25.4.5 Properties of the Promise Prototype Object', function() {
  it('is an ordinary object', function() {
    const p = new MyPromise(resolve => resolve());

    expect(Object.getPrototypeOf(p)).instanceOf(Object);
  });
});
