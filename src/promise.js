const state = Symbol();
const queue = Symbol();
const _resolve = Symbol();
const _reject = Symbol();
const _runQueue = Symbol();
const _settle = Symbol();
const _checkState = Symbol();

class MyPromise {
  constructor(executor) {
    this[state] = 'pending';
    this.value;
    this[queue] = [];

    if (typeof executor !== 'function') {
      throw new TypeError();
    }
    try {
      executor(this[_resolve].bind(this), this[_reject].bind(this));
    } catch (e) {
      this[_reject](e);
    }
  }

  [_resolve](value) {
    if (this.constructor !== MyPromise && this !== MyPromise) {
      throw new TypeError();
    }

    if (this[state] !== 'pending') {
      return;
    }
    this[state] = 'resolved';

    if (value instanceof MyPromise) {
      value._checkState(
        val => this[_settle](val),
        error => {
          this[state] = 'rejected';
          this[_settle](error);
        }
      );
    } else {
      this[_settle](value);
    }
  }

  [_reject](reason) {
    if (this[state] !== 'pending') {
      return;
    }
    this[state] = 'rejected';
    this[_settle](reason);
  }

  [_runQueue](resolutionHandler, rejectionHandler) {
    if (this[state] === 'rejected') {
      if (typeof rejectionHandler !== 'function') {
        throw this._value;
      } else {
        rejectionHandler(this.value);
      }
    } else if (typeof resolutionHandler !== 'function') {
      throw this._value;
    } else {
      resolutionHandler(this.value);
    }
  }

  [_settle](value) {
    this.value = value;
    setTimeout(() => {
      this[queue].forEach(item => {
        this[_runQueue](item.resolutionHandler, item.rejectionHandler);
      });
    }, 0);
  }

  [_checkState](resolutionHandler, rejectionHandler) {
    if (this[state] !== 'pending') {
      setTimeout(() => this[_runQueue](resolutionHandler, rejectionHandler), 0);
    } else {
      this[queue].push({ resolutionHandler, rejectionHandler });
    }
  }

  then(resolutionHandler, rejectionHandler) {
    if (this.constructor !== MyPromise && this !== MyPromise) {
      throw new TypeError();
    }
    return new MyPromise((resolve, reject) => {
      this[_checkState](
        value => {
          try {
            value = resolutionHandler(value);
          } catch (e) {
            reject(e);
          }
          resolve(value);
        },
        error => {
          if (typeof rejectionHandler === 'function') {
            try {
              error = rejectionHandler(error);
            } catch (e) {
              reject(e);
            }
            resolve(error);
          }
          reject(error);
        }
      );
    });
  }

  catch(rejectionHandler) {
    return this.then(null, rejectionHandler);
  }

  static resolve(value) {
    if (this.constructor !== MyPromise && this !== MyPromise) {
      throw new TypeError();
    }

    if (value instanceof MyPromise) {
      return value;
    } else {
      return new MyPromise(res => {
        res(value);
      });
    }
  }

  static reject(reason) {
    if (this.constructor !== MyPromise && this !== MyPromise) {
      throw new TypeError();
    }
    return new MyPromise((resolve, reject) => reject(reason));
  }

  static all(iterable) {
    if (this.constructor !== MyPromise && this !== MyPromise) {
      throw new TypeError();
    }

    if (typeof iterable[Symbol.iterator] !== 'function') {
      return new MyPromise((resolve, reject) => {
        reject(new TypeError());
      });
    }

    const results = [];
    let completedPromises = 0;

    return new MyPromise((resolve, reject) => {
      if (iterable.length) {
        iterable.forEach((promise, index) => {
          promise
            .then(value => {
              results[index] = value;
              completedPromises += 1;

              if (completedPromises === iterable.length) {
                resolve(results);
              }
            })
            .catch(error => {
              reject(error);
            });
        });
      } else if (iterable.length === 0) {
        return resolve([]);
      }
    });
  }

  static race(iterable) {
    if (this.constructor !== MyPromise && this !== MyPromise) {
      throw new TypeError();
    }

    if (typeof iterable[Symbol.iterator] !== 'function') {
      return new MyPromise((resolve, reject) => {
        reject(new TypeError());
      });
    }
    return new MyPromise((resolve, reject) => {
      iterable.forEach(promise => {
        promise.then(resolve);
        promise.catch(reject);
      });
    });
  }
}
module.exports = MyPromise;
